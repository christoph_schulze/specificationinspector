import {Component, OnInit} from '@angular/core';
import {MdSnackBar, MdSnackBarConfig} from '@angular/material';
import { Router } from '@angular/router';

import {DataService} from '../../services/data.service';
import {ParserService} from '../../services/parser.service';
import {InitializationService} from '../../services/initialization.service';
import {ComparisonService} from '../../services/comparison.service';
import { PropertyFile } from '../../models/property-file';


@Component({
  moduleId: module.id,
  selector: 'my-file-manager',
  templateUrl: 'file-manager.component.html',
  styleUrls: ['./file-manager.component.css']
})
export class FileManagerComponent implements OnInit {
  files: any;

  private firstFile: PropertyFile = null;
  private secondFile: PropertyFile = null;

  constructor(public _dataService: DataService, public _parserService: ParserService,
    private _initializationService: InitializationService, public _comparisonService: ComparisonService, public snackBar: MdSnackBar, private _router: Router, ) { }

  ngOnInit() {
    //this._parserService.init(this._initializationService);
  }

  changeListener($event): void {
    this.setFiles($event.target);
  }

  setFiles(inputValue: any): void {
    this.files = inputValue.files;
  }

  loadFiles(): void {
    if (this.files !== null) {
      for (let file of this.files) {
        this.readFile(file);
      }
    }
  }

  readFile(file: File): void {
    let myReader = new FileReader();
    let _parserService = this._parserService;

    myReader.onloadend = function (e) {
      // you can perform an action with data read here
      // as an example i am just splitting strings by spaces
      _parserService.addFile(file.name, myReader.result);
      console.log('came here');
    };

    myReader.readAsText(file);
  }

  compare(){
    let errorMessage = '';
    if(this.firstFile == null && this.secondFile == null){
      errorMessage = 'No files have been selected';
    } else if(this.firstFile == null){
      errorMessage = 'Please select the first file';
    } else if(this.secondFile == null){
      errorMessage = 'Please select the second file';
    } else if(this.firstFile.fileName == this.secondFile.fileName){
      errorMessage = 'The files are the same!';
    }

    if(errorMessage != ""){
      let config = new MdSnackBarConfig();
      config.duration = 3000;
      this.snackBar.open(errorMessage, null, config);
    }else{
      this._comparisonService.compare(this.firstFile, this.secondFile);
      this._router.navigate(['/diff']);
    }
  }

}
