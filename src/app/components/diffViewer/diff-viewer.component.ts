import {Component, OnInit, ViewChild, AfterViewInit} from '@angular/core';
import {MdSnackBar, MdTab, MdTabGroup} from '@angular/material';

import {DataService} from '../../services/data.service';
import {ComparisonService} from '../../services/comparison.service';
import { PropertyFile } from '../../models/property-file';

@Component({
  moduleId: module.id,
  selector: 'my-diff-viewer',
  templateUrl: 'diff-viewer.component.html',
  styleUrls: ['diff-viewer.component.css']
})
export class DiffViewerComponent implements OnInit, AfterViewInit {
  private firstFile: PropertyFile = null;
  private secondFile: PropertyFile = null;
  private inBoth = 0;
  private inFirst = 0;
  private inSecond = 0;

  @ViewChild('filter') sidebar;

  @ViewChild('tabGroup') group: MdTabGroup;



  constructor(public _dataService: DataService, public _comparisonService: ComparisonService, public snackBar: MdSnackBar) { }

  ngOnInit() {
    //this._parserService.init(this._initializationService);
  }

  selectionChanged(){
    switch (this.group.selectedIndex) {
      case 0:
        this._dataService.showTheseProperties(this._comparisonService.inBoth);
        break;
      case 1:
        this._dataService.showTheseProperties(this._comparisonService.onlyInFirst);
        break;
      case 2:
        this._dataService.showTheseProperties(this._comparisonService.onlyInSecond);
        break;

      default:
        break;
    }
    console.log("came here: " + this.group.selectedIndex);
  }

  ngAfterViewInit() {
    this._dataService.showTheseProperties(this._comparisonService.inBoth);
  }

  compare() {
    let errorMessage = '';
    if(this.firstFile == null && this.secondFile == null){
      errorMessage = 'No file has been selected';
    } else if(this.firstFile == null){
      errorMessage = 'Please select the first file';
    } else if(this.secondFile == null){
      errorMessage = 'Please select the second file';
    } else if(this.firstFile.fileName == this.secondFile.fileName){
      errorMessage = 'The files are the same!';
    }

    if(errorMessage != ""){
      this.snackBar.open(errorMessage);
    }else{
      this._comparisonService.compare(this.firstFile, this.secondFile);

      this.inBoth = this._comparisonService.inBoth.size;
      this.inFirst = this._comparisonService.onlyInFirst.size;
      this.inSecond = this._comparisonService.onlyInSecond.size;

      this.sidebar.close();
    }

  }

}
