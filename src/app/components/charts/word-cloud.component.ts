import { Component, OnInit, AfterViewInit, OnDestroy, NgZone } from '@angular/core';

import { DataService } from '../../services/data.service';
import { FilterService } from '../../services/filter.service';
import { Property } from '../../models/property';
import { Chart } from '../../models/chart';

declare var zingchart: any;
declare var Plotly: any;

@Component({
  moduleId: module.id,
  selector: 'word-cloud',
  templateUrl: 'word-cloud.component.html',
  styleUrls: ['word-cloud.component.css'],
})


export class WordCloudComponent implements OnInit, AfterViewInit, OnDestroy {
  constructor(public _dataService: DataService, public _filterService: FilterService, private zone: NgZone) { }

  ngAfterViewInit () {
    zingchart.THEME="classic";

    var myConfig = {
        "background-color":"#ffffff",
        "graphset":[
            {
                "type":"wordcloud",
                "x":"0%",
                "y":"0%",
                "width":"50%",
                "height":"100%",
                "background-color":"#F2F3F8",
                "title":{
                    "text":"LHS Values",
                    "y":"5%",
                    "background-color":"none",
                    "font-size":"16px",
                    "font-color":"#000000"
                },
                "plotarea":{
                    "margin":"50 auto auto auto"
                },
                "options":{
                    "max-items":20,
                    "font-family":"Arial",
                    "rotate":true,
                    "min-length":4,
                    "min-font-size":"9px",
                    "max-font-size":"30px",
                    "color-type":"palette",
                    "palette":["#57aa83","#c13f43","#2c497d","#8965ad","#686d7b","#fd625e","#d7d7d9","#2e67c6"],
                    "text":""
                }
            },
            {
                "type":"wordcloud",
                "x":"51%",
                "y":"0%",
                "width":"50%",
                "height":"100%",
                "background-color":"#F2F3F8",
                "title":{
                    "text":"RHS Values",
                    "y":"5%",
                    "background-color":"none",
                    "font-size":"16px",
                    "font-color":"#000000"
                },
                "plotarea":{
                    "margin":"50 auto auto auto"
                },
                "options":{
                    "max-items":20,
                    "font-family":"Arial",
                    "rotate":true,
                    "min-length":4,
                    "min-font-size":"9px",
                    "max-font-size":"30px",
                    "color-type":"palette",
                    "palette":["#57aa83","#c13f43","#2c497d","#8965ad","#686d7b","#fd625e","#d7d7d9","#2e67c6"],
                    "text":""
                }
            }
        ]
    };

    myConfig.graphset[0].options.text = this.getLHS();
    myConfig.graphset[1].options.text = this.getRHS();

    this.zone.runOutsideAngular(() => {
              zingchart.render({
                  id : 'myChart',
                  data : myConfig,
                  width : 1000,
                  height: 725
              });
          });

    var data = [
  {
    z: [[1, 20, 30, 50, 1], [20, 1, 60, 80, 30], [30, 60, 1, -10, 20]],
    x: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'],
    y: ['Morning', 'Afternoon', 'Evening'],
    type: 'heatmap'
  }
];
this.zone.runOutsideAngular(() => {
Plotly.newPlot('myDiv', data);
});
	}

	ngOnDestroy () {
		zingchart.exec('myChart', 'destroy');
}

  ngOnInit() {



  }

  getLHS(): string{
    let result = '';
    this._dataService.propertyMap.forEach(property => {
      property.lhs.forEach(nameValue => {
        result += nameValue.name + '_' + nameValue.value + ' ';
      });
    });
    return result;
  }

  getRHS(): string{
    let result = '';
    this._dataService.propertyMap.forEach(property => {
      property.rhs.forEach(nameValue => {
        result += nameValue.name + '_' + nameValue.value + ' ';
      });
    });
    return result;
  }

  getFiles (property: Property): string {
    let result = '';

    for (let index = 0; index < property.statistics.fileIds.length; index++) {
      let file = property.statistics.fileIds[index];

      if (index === 0) {
        result += file;
      } else {
        result += + '\n' + file;
      }
    }

    return result;
  }

}
