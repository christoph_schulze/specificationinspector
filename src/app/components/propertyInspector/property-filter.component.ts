import { Component, OnInit, EventEmitter, Output } from '@angular/core';

import {DataService} from '../../services/data.service';
import {FilterService} from '../../services/filter.service';

@Component({
  moduleId: module.id,
  selector: 'my-property-filter',
  templateUrl: 'property-filter.component.html',
  styleUrls: ['property-filter.component.css']
})

export class PropertyFilterComponent implements OnInit {
  @Output() onClose = new EventEmitter<void>();

  filterLHS:string = "";
  filterRHS:string = "";

  constructor(private _dataService: DataService, private _filterService: FilterService) { }

  ngOnInit() {

  }

  applyFilter() {
    this._filterService.applyFilter(this.filterLHS, this.filterRHS);
    this.onClose.emit();
  }

  resetFilter() {
    this._filterService.resetFilter();
    this.onClose.emit();
  }

}
