
import { Component, OnInit } from '@angular/core';


@Component({
  moduleId: module.id,
  selector: 'my-property-viewer-filter',
  templateUrl: 'property-viewer-filter.component.html',
  styleUrls: ['property-viewer-filter.component.css'],
})

export class PropertyViewerFilterComponent implements OnInit {
  constructor() { }

  ngOnInit() {
  }

}
