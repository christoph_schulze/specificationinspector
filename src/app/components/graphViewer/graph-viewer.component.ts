import { Component, ViewChild, OnInit, AfterViewInit, NgZone } from '@angular/core';

import { DataService } from '../../services/data.service';
import { StatechartService } from '../../services/statechart.service';
import { Property } from '../../models/property';
import { Variable } from '../../models/variable';
import { VariablePair } from '../../models/variable-pair';


declare var d3: any;
declare var dagreD3: any;
declare var graphlibDot: any;


@Component({
  moduleId: module.id,
  selector: 'my-graph-viewer',
  templateUrl: 'graph-viewer.component.html',
  styleUrls: ['graph-viewer.component.css'],
})


export class GraphViewerComponent implements OnInit, AfterViewInit {
  @ViewChild('svg') svg;
  @ViewChild('filter') sidebar;

  constructor(public _dataService: DataService, public _statechartService: StatechartService, private zone: NgZone) { }

  private pairs: VariablePair[] = [{
    firstVariable: 'var1', secondVariable: 'oldVar1'
  },
  {
    firstVariable: 'var2', secondVariable: 'oldVar2'
  }];

  private variables: Variable[] = [];
  private ignores: String[] = [];

  private selectedValueFirst: string = '';
  private selectedValueSecond: string = '';
  private selectedValueIgnore: string = '';

  private render;

  ngOnInit() {
    this.loadVariables();
  }


  ngAfterViewInit(): void {
    this.zone.runOutsideAngular(() => {
      // Set up zoom support
      var svg = d3.select("svg");
      var inner = d3.select("svg g");
      var zoom = d3.behavior.zoom().on("zoom", function () {
          inner.attr("transform", "translate(" + d3.event.translate + ")" +
            "scale(" + d3.event.scale + ")");
        });
      svg.call(zoom);

      // Create and configure the renderer
      this.render = dagreD3.render();
    });
  }

  createStateChart(): void {
    let text = '';
    text = this._statechartService.createStateChart(this.pairs, this.ignores);
    console.log(text);
    this.tryDraw(text);
    this.sidebar.close();
  }

  removeVariables(): void {
    this.pairs.length = 0;
  }

  addToIgnore(): void {
    if (this.selectedValueIgnore == '') {
      return;
    }

    if(!this.ignores.includes(this.selectedValueIgnore)){
      this.ignores.push(this.selectedValueIgnore);
    }


  }

  addVariables(): void {
    if (this.selectedValueFirst == '' || this.selectedValueSecond == '') {
      return;
    }

    let pair = new VariablePair();
    pair.firstVariable = this.selectedValueFirst;
    pair.secondVariable = this.selectedValueSecond;

    this.pairs.push(pair);

  }

  loadVariables(): void {
    //this.firstVariables.concat(this._dataService.variableMap.entries;
    this._dataService.variableMap.forEach(variable => {
      this.variables.push(variable);
    });
  }

  loadText(): string {
    let text = '';
    this._dataService.variableMap.forEach(element => {

    });


    return text;
  }

  getFiles(property: Property): string {
    let result = '';

    for (let index = 0; index < property.statistics.fileIds.length; index++) {
      let file = property.statistics.fileIds[index];

      if (index === 0) {
        result += file;
      } else {
        result += + '\n' + file;
      }
    }

    return result;
  }



  tryDraw(graphString: string) {
    this.zone.runOutsideAngular(() => {

      var g;
      try {
        g = graphlibDot.read(graphString);
      } catch (e) {
        throw e;
      }

      // Set margins, if not present
      if (!g.graph().hasOwnProperty("marginx") &&
        !g.graph().hasOwnProperty("marginy")) {
        g.graph().marginx = 20;
        g.graph().marginy = 20;
      }

      g.graph().transition = function (selection) {
        return selection.transition().duration(500);
      };

      // Render the graph into svg g
      d3.select("svg g").call(this.render, g);
    });
  }

}
