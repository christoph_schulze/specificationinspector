import { Component, OnInit  } from '@angular/core';
import { Router } from '@angular/router';

import { InitializationService } from './services/initialization.service';
import { ParserService } from './services/parser.service';
import { DataService } from './services/data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {

  constructor(private _router: Router, public _parserService: ParserService, public _initializationService: InitializationService,
    private _dataService: DataService) { };

  ngOnInit() {
    this._parserService.init(this._initializationService);
  }

  onClose() {

  }

  goToProperties() {
    //this.sidenav.close();
    this._dataService.resetProperties();
    this._router.navigate(['/properties']);
    //console.log('test');
  }

  goToGraph() {
    //this.sidenav.close();
    this._router.navigate(['/statemachine']);
    //console.log('test');
  }

  goToCharts() {
    //this.sidenav.close();
    this._router.navigate(['/charts']);
    //console.log('test');
  }

  goToFiles() {
    //this.sidenav.close();
    this._router.navigate(['/filemanager']);
    //console.log('test1');
  }

  goToHome(){
    this._router.navigate(['/examples']);
  }

    goToDiff() {
    //this.sidenav.close();
    this._router.navigate(['/diff']);
    //console.log('test1');
  }

}
