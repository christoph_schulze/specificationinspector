import {Pipe, PipeTransform} from '@angular/core';

import { Property } from '../../models/property';
import { NameValue } from '../../models/name-value';
import { DataService } from '../../services/data.service';

@Pipe({name: 'sortProperties', pure:false})
export class SortPropertiesPipe implements PipeTransform {
  constructor(public _dataService: DataService) { }

    transform(value: any, args?: any[]): Object[] {
        let returnArray = [];

        value.forEach((id) => {
          let property = this._dataService.propertyMap.get(id);
            returnArray.push(property);
        });

        return returnArray.sort((p1, p2) => this.sortProperty(p1, p2));
    }

  sortNameValue(n1: NameValue, n2: NameValue) {
    if (n1.name > n2.name) {
      return 1;
    }

    if (n1.name < n2.name) {
      return -1;
    }

    if (n1.value > n2.value) {
      return 1;
    }

    if (n1.value < n2.value) {
      return -1;
    }

    return 0;
  }

  sortNameValues(n1: NameValue[], n2: NameValue[]) {
    for (let index = 0; index < n1.length; index++) {
      let nameValue1 = n1[index];
      let nameValue2 = n2[index];

      let result = this.sortNameValue(nameValue1, nameValue2);
      if (result !== 0 ) {
        return result;
      }
    }
    return 0;
  }

  sortProperty(p1: Property, p2: Property) {
    if (p1.lhs.length > p2.lhs.length) {
      return 1;
    }
    if (p1.lhs.length < p2.lhs.length) {
      return -1;
    }

    return this.sortNameValues(p1.lhs, p2.lhs);
  }
}
