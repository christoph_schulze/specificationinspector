import { Routes, RouterModule } from '@angular/router';

import { PropertyViewerFilterComponent } from './components/propertyInspector/property-viewer-filter.component';
import { GraphViewerComponent } from './components/graphViewer/graph-viewer.component';
import { FileManagerComponent } from './components/fileManager/file-manager.component';
import { ChartComponent } from './components/charts/chart.component';
import { DiffViewerComponent } from './components/diffViewer/diff-viewer.component';
import { StartScreenComponent } from './components/startScreen/start-screen.component';

const appRoutes: Routes = [
  { path: 'examples', component: StartScreenComponent },
  { path: 'properties', component: PropertyViewerFilterComponent },
  { path: '', redirectTo: '/examples', pathMatch: 'full' },
  { path: 'filemanager', component: FileManagerComponent },
  { path: 'statemachine', component: GraphViewerComponent },
  { path: 'charts', component: ChartComponent },
  { path: 'diff', component: DiffViewerComponent }
];

export const appRoutingProviders: any[] = [

];

export const routing = RouterModule.forRoot(appRoutes);
