import {NameValue} from '../name-value';

export interface StatechartElement{
  id: NameValue[];
  name: string;
}
