import {State} from './state';
import {Transition} from './transition';

export class StateChart{
  transitions: Array<Transition> = new Array<Transition>();
  states: Array<State> = new Array<State>();

  printDOT():string{
    let dot = 'digraph G{\n';

    this.transitions.forEach(transition => {
      dot += transition.srcId + ' -> ' + transition.targetId + ' [label="' + this.getName(transition) + '"];\n';
    });

    this.states.forEach(state => {
      dot += state.printId() + ' [label= "' + state.name + '"];\n'
    });

    dot += '}';

    return dot;
  }

  getName(transition: Transition):string{
    let result = '';
    let first = true;
    transition.name.forEach(name => {
      if(first){
        first = false;
        result += name;
      }
      else{
        result += ' || \n ' + name;
      }
    });

    return result;
  }
}
