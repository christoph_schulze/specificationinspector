
export class Transition{
  id: string;
  name: string[] = [];
  srcId: string;
  targetId: string;
}
