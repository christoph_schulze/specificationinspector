import { Value } from './value';

export class Variable {
  name: string;
  valuesMap: Map<string, Value> = new Map<string, Value>();
  filtered: boolean;
  propertyIds: Array<string> = new Array<string>();
  isLHS: boolean = true;

  // isTheSameVariable( variable: Variable): boolean {
  //    if ( this.name === variable.name) {
  //       if ( this.value === variable.value) {
  //         return true;
  //       }
  //     }
  //     return false;
  // }
}
