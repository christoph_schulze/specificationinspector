import { Variable } from './variable';

export class VariablePair {
  firstVariable: string;
  secondVariable: string;
}
