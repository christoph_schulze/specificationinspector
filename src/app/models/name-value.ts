export class NameValue {
  name: string;
  value: string;

  equals(toCompare: NameValue): boolean{
    if(this.name == toCompare.name && this.value == toCompare.value){
      return true;
    }
    return false;
  }
}
