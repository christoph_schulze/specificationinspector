import { Injectable } from '@angular/core';
import {Http, Response } from '@angular/http';

import {ParserService} from './parser.service'

const URL_PROPERTIES_CRUISE1 = 'assets/data/cruise1.txt';
const URL_PROPERTIES_CRUISE2 = 'assets/data/cruise2.txt';
const URL_PROPERTIES_HEART1 = 'assets/data/path1.txt';
const URL_PROPERTIES_HEART2 = 'assets/data/path2.txt';
const URL_PROPERTIES_PACEMAKER = 'assets/data/lri.txt';
const URL_PROPERTIES_TEST = 'assets/data/test.txt';


@Injectable()
export class InitializationService {

  constructor(private _http: Http, private _parser: ParserService) { }
  // variables: VariableValues[] = [
  //     { name: 'Var1', valuesAndOccurences: [{ value: 'test', occurence: 1, checked: true}, { value: 'test2', occurence: 12, checked: true}] },
  //     { name: 'Var2', valuesAndOccurences: [{ value: 'test', occurence: 1, checked: true}] },
  //     { name: 'Var3', valuesAndOccurences: [{ value: 'test', occurence: 1, checked: true}] }
  // ];

  // constructor(private _http: Http) { }

  loadHeart(){
    this.getPropertiesHeart1()
      .then((data) => {
        this._parser.addFile('path1.txt', data);
      })
      .catch((err) => {
        console.log('Could not load properties');
        throw err;
        // console.log(err); // dont do this, show the user a nicer message
      });
      this.getPropertiesHeart2()
      .then((data) => {
        this._parser.addFile('path2.txt', data);
      })
      .catch((err) => {
        console.log('Could not load properties');
        throw err;
        // console.log(err); // dont do this, show the user a nicer message
      });
  }

  loadPacemaker(){
    this.getPropertiesPacemaker()
      .then((data) => {
        this._parser.addFile('pacemaker.txt', data);
      })
      .catch((err) => {
        console.log('Could not load properties');
        throw err;
        // console.log(err); // dont do this, show the user a nicer message
      });
  }

  loadTest(){
    this.getPropertiesTest()
      .then((data) => {
        this._parser.addFile('test.txt', data);
      })
      .catch((err) => {
        console.log('Could not load properties');
        throw err;
        // console.log(err); // dont do this, show the user a nicer message
      });
  }

  loadCruise(){
    this.getPropertiesCruise1()
      .then((data) => {
        this._parser.addFile('cruise1.txt', data);
      })
      .catch((err) => {
        console.log('Could not load properties');
        throw err;
        // console.log(err); // dont do this, show the user a nicer message
      });

      /**this.getPropertiesCruise2()
      .then((data) => {
        this._parser.addFile('cruise2.txt', data);
      })
      .catch((err) => {
        console.log('Could not load properties');
        throw err;
        // console.log(err); // dont do this, show the user a nicer message
      });*/
  }

  getPropertiesPacemaker() {
      return this._http.get(URL_PROPERTIES_PACEMAKER).map((response: Response) => response.text())
      .toPromise()
      .catch((err: any) => {
       console.log(err); // log this somewhere and format the message well for devs
        return Promise.reject(err); // our opportunity to customize this error
      });
   }

   getPropertiesTest() {
      return this._http.get(URL_PROPERTIES_TEST).map((response: Response) => response.text())
      .toPromise()
      .catch((err: any) => {
       console.log(err); // log this somewhere and format the message well for devs
        return Promise.reject(err); // our opportunity to customize this error
      });
   }

   getPropertiesCruise1() {
      return this._http.get(URL_PROPERTIES_CRUISE1).map((response: Response) => response.text())
      .toPromise()
      .catch((err: any) => {
       console.log(err); // log this somewhere and format the message well for devs
        return Promise.reject(err); // our opportunity to customize this error
      });
   }

   getPropertiesCruise2() {
      return this._http.get(URL_PROPERTIES_CRUISE2).map((response: Response) => response.text())
      .toPromise()
      .catch((err: any) => {
       console.log(err); // log this somewhere and format the message well for devs
        return Promise.reject(err); // our opportunity to customize this error
      });
   }

   getPropertiesHeart1() {
      return this._http.get(URL_PROPERTIES_HEART1).map((response: Response) => response.text())
      .toPromise()
      .catch((err: any) => {
       console.log(err); // log this somewhere and format the message well for devs
        return Promise.reject(err); // our opportunity to customize this error
      });
   }

   getPropertiesHeart2() {
      return this._http.get(URL_PROPERTIES_HEART2).map((response: Response) => response.text())
      .toPromise()
      .catch((err: any) => {
       console.log(err); // log this somewhere and format the message well for devs
        return Promise.reject(err); // our opportunity to customize this error
      });
   }

  // getLHSVariableValues(): VariableValues[] {
  //   return this.variables;
  // }

  // getRHSVariableValues(): VariableValues[] {
  //   return this.variables;
  // }
}
