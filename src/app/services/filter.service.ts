import { Injectable } from '@angular/core';

import { DataService } from './data.service';
import { Property } from '../models/property';
import { Variable } from '../models/Variable';
import { Value } from '../models/Value';
import { NameValue } from '../models/name-value';

@Injectable()
export class FilterService {
  combine = false;

  nameFilters: Array<string> = new Array<string>();
  valueFilters: Array<NameValue> = new Array<NameValue>();

  filterLHS:string = "";
  filterRHS:string = "";

  constructor(private _dataService: DataService) { }

  applyFilter(filterLHS:string, filterRHS:string) {
    this.valueFilters.length = 0;
    this.nameFilters.length = 0;
    this.filterLHS = filterLHS;
    this.filterRHS = filterRHS;
    this.filter();
  }

  filterForVariable(variable: NameValue) {
    if (this.combine) {
      if (this.valueFilters.indexOf(variable) === -1) {
        this.valueFilters.push(variable);
      }
    } else {
      this.valueFilters.length = 0;
      this.nameFilters.length = 0;
      this.valueFilters.push(variable);
    }
    this.filter();
  }

  filterForVariableName(name: string) {
    if (this.combine) {
      if (this.nameFilters.indexOf(name) === -1) {
        this.nameFilters.push(name);
      }
    } else {
      this.valueFilters.length = 0;
      this.nameFilters.length = 0;
      this.nameFilters.push(name);
    }
    this.filter();
  }

  resetFilter() {
    this.valueFilters.length = 0;
    this.nameFilters.length = 0;
    this.filter();
  }

  filter() {
    let iterator = this._dataService.propertyMap.entries();

    let propertySet = iterator.next().value;
    while (propertySet != null) {
      if (this.shouldFilter(propertySet[1])) {
        propertySet[1].filtered = true;
      } else {
        propertySet[1].filtered = false;
      }

      propertySet = iterator.next().value;
    }
  }

  shouldFilter(property: Property): boolean {
    // Value Filters
    let filtered = this.valueFilter(property);

    if (filtered) {
      return true;
    }

    // Name Filter
    filtered = this.nameFilter(property);

    if (filtered) {
      return true;
    }

    return false;
  }

  valueFilter(property: Property): boolean {
    for (let variable of this.valueFilters) {
      let filterIt = true;
      for (let lhsVariable of property.lhs) {
        if (lhsVariable.name === variable.name && lhsVariable.value === variable.value) {
          filterIt = false;
        }
      }
      for (let lhsVariable of property.rhs) {
        if (lhsVariable.name === variable.name && lhsVariable.value === variable.value) {
          filterIt = false;
        }
      }
      if (filterIt) {
        return true;
      }
    }
    return false;
  }

  nameFilter(property: Property): boolean {
    for (let name of this.nameFilters) {
      let filterIt = true;
      for (let lhsVariable of property.lhs) {
        if (lhsVariable.name === name) {
          filterIt = false;
        }
      }
      for (let rhsVariable of property.rhs) {
        if (rhsVariable.name === name) {
          filterIt = false;
        }
      }
      if (filterIt) {
        return true;
      }
    }

    let foundLHSMatch = false;
    for (let lhsVariable of property.lhs) {
      if(lhsVariable.name.includes(this.filterLHS)){
        foundLHSMatch = true;
      }
    }
    let foundRHSMatch = false;
    for (let rhsVariable of property.rhs) {
      if(rhsVariable.name.includes(this.filterRHS)){
        foundRHSMatch = true;
      }
    }

    if((this.filterLHS != "" &&  !foundLHSMatch) || (this.filterRHS != "" &&  !foundRHSMatch)){
      return true;
    }
    return false;
  }
}
