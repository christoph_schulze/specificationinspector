import { Injectable } from '@angular/core';

import { DataService } from './data.service';
import { InitializationService } from './initialization.service';
import { PropertyFile } from '../models/property-file';
import { NameValue } from '../models/name-value';

@Injectable()
export class ParserService {
  constructor(private _dataService: DataService) { }

  init(initializationService: InitializationService) {

  }

  addFile(fileName: string, data: string) {
    let exists = false;
    for (let file of this._dataService.files) {
      if (file.fileName === fileName) {
        this._dataService.removePropertyReferences(file);
        exists = true;
        break;
      }
    }
    if (!exists) {
      let propertyFile = new PropertyFile();
      propertyFile.fileName = fileName;
      this._dataService.files.push(propertyFile);
    }
    this.parseFile(fileName, data);
  }

  parseFile(fileName: string, data: string) {
    data.split(/\r|\n/).forEach(line => {
      line = line.trim();
      if (line === '') {
        return;
      }
      this.parseProperty(line, fileName);
    });

  }

  parseProperty(line: string, fileName: string) {
    //let lhsRhsAndMetaData = line.split();
    let lhsRhsArray = line.split('###')[0].split('->');

    if (lhsRhsArray.length !== 2) {
      return;
    }

    let lhs = this.parseVariables(lhsRhsArray[0]).sort((n1, n2) => this.sort(n1, n2));
    let rhs = this.parseVariables(lhsRhsArray[1]).sort((n1, n2) => this.sort(n1, n2));

    // Add new property
    this._dataService.addProperty(lhs, rhs, fileName);
  }

  parseVariables(dataString: string): NameValue[] {
    let variables = new Array<NameValue>();

    let variablesString = dataString.split('&');

    variablesString.forEach(variableString => {
      let nameValueArray = variableString.split('==');
      if (nameValueArray.length !== 2) {
        return;
      }
      let nameValue = new NameValue();
      nameValue.name = nameValueArray[0].trim();
      nameValue.value = nameValueArray[1].trim();
      variables.push(nameValue);
    });

    return variables;
  }


  sort(n1: NameValue, n2: NameValue) {
    if (n1.name > n2.name) {
      return 1;
    }

    if (n1.name < n2.name) {
      return -1;
    }

    return 0;
  }
}
