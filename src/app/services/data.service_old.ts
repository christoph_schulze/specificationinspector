// import { Injectable, OnInit } from '@angular/core';

// import { PropertyService } from './property.service';
// import { Property } from '../models/property';
// import { Variable } from '../models/variable';
// import { VariableValues } from '../models/variable-values';

// @Injectable()
// export class DataService  implements OnInit {
//   properties: Property[];
//   visibleProperties: Property[];
//   variableValuesLHS: VariableValues[];
//   variableValuesRHS: VariableValues[];

//   //  variableValuesLHS: Map<string, VariableValues> = new Map<string, VariableValues>();
//   //  variableValuesRHS: Map<string, VariableValues> = new Map<string, VariableValues>();

//   combine = false;

//   constructor(public _propertyService: PropertyService) { }

//   ngOnInit() {
//     this._propertyService.getProperties()
//       .then((properties) => {
//         this.properties = properties;
//         this.visibleProperties = properties;
//         //this.initializeVariables();
//       })
//       .catch((err) => {
//         console.log('Could not load properties');
//         // console.log(err); // dont do this, show the user a nicer message
//       });

//       //this.initializeVariables();
//   }

//   init() {
//     this._propertyService.getProperties()
//       .then((properties) => {
//         this.properties = properties;
//         this.visibleProperties = properties;
//         //this.initializeVariables();
//       })
//       .catch((err) => {
//         console.log('Could not load properties');
//         // console.log(err); // dont do this, show the user a nicer message
//       });

//       this.variableValuesLHS = this._propertyService.getLHSVariableValues();
//       this.variableValuesRHS = this._propertyService.getRHSVariableValues();

//       // this._propertyService.getVariableValues()
//       // .then((values) => {
//       //   this.variableValuesLHS = values[0];
//       //   this.variableValuesRHS = values[1];
//       // })
//       // .catch((err) => {
//       //   console.log('Could not load variableValues');
//       //   // console.log(err); // dont do this, show the user a nicer message
//       // });
//   }

//   // initializeVariables() {
//   //   this.properties.forEach(property => {
//   //     property.lhs.forEach(variable => {
//   //       this.countVariableValues(variable, this.variableValuesLHS);
//   //     });
//   //     property.rhs.forEach(variable => {
//   //       this.countVariableValues(variable, this.variableValuesRHS);
//   //     });
//   //   });
//   // }

//   // countVariableValues(variable: Variable, variableValues: Map<string, VariableValues>) {
//   //   let variableValue = variableValues.get(variable.name);
//   //   if ( variableValue === null ) {
//   //     variableValue = new VariableValues();
//   //     variableValue.name = variable.name;
//   //     variableValues.set(variableValue.name, variableValue);
//   //   }
//   //   variableValue.addVariableOccurence(variable);
//   // }

//   containsVariable(property: Property, variable: Variable): boolean {
//     let index = property.lhs.findIndex((isVar) => variable.name === isVar.name && variable.value === isVar.value);

//     if ( index !== -1 ) {
//       return true;
//     }

//     index = property.rhs.findIndex((isVar) => variable.name === isVar.name && variable.value === isVar.value);

//     if ( index !== -1 ) {
//       return true;
//     }
//   }

//   containsVariableName(property: Property, name: string): boolean {
//     let index = property.lhs.findIndex((isVar) => isVar.name === name);

//     if ( index !== -1 ) {
//       return true;
//     }

//     index = property.rhs.findIndex((isVar) => isVar.name === name);

//     if ( index !== -1 ) {
//       return true;
//     }
//   }

//   filterForVariable(variable: Variable) {
//     if ( this.combine ) {
//       this.visibleProperties = this.visibleProperties.filter((property) => {
//           // console.log(variable);
//           return this.containsVariable(property, variable);
//       });
//     } else {
//       this.visibleProperties = this.properties.filter((property) => {
//           // console.log(variable);
//           return this.containsVariable(property, variable);
//       });
//     }
//   }

//   filterRHS(variable: Variable[]) {
//     console.log('test');
//   }

//   filterLHS(variable: Variable[]) {
//     console.log('test');
//     // if ( this.combine ) {
//     //   this.visibleProperties = this.visibleProperties.filter((property) => {
//     //       // console.log(variable);
//     //       return this.containsVariable(property, variable);
//     //   });
//     // } else {
//     //   this.visibleProperties = this.properties.filter((property) => {
//     //       // console.log(variable);
//     //       return this.containsVariable(property, variable);
//     //   });
//     // }
//   }

//   filterForVariableName(name: string) {
//     if ( this.combine ) {
//       this.visibleProperties = this.visibleProperties.filter((property) => {
//         // console.log(name);
//         return this.containsVariableName(property, name);
//     });
//     } else {
//       this.visibleProperties = this.properties.filter((property) => {
//         // console.log(name);
//         return this.containsVariableName(property, name);
//     });
//     }
//   }

//   resetFilter() {
//     this.visibleProperties = this.properties;
//   }

//   filter() {

//   }

// }
