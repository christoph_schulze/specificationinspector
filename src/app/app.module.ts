//8c315ee7-e4d1-45f0-941f-087029da2ee3

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MaterialModule } from '@angular/material';
import {CommonModule} from '@angular/common';
import 'hammerjs';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

// Material 2
import { MdToolbarModule, MdTooltipModule, MdButtonModule, MdSidenavModule, MdListModule, MdGridListModule, MdCardModule, MdInputModule,
MdCheckboxModule, MdRadioModule, MdIconModule, MdMenuModule, MdCoreModule} from '@angular/material/';
import { provideRoutes} from '@angular/router';


// Components
import { AppComponent }  from './app.component';
import { PropertyViewerComponent } from './components/propertyInspector/property-viewer.component';
import { PropertyFilterComponent } from './components/propertyInspector/property-filter.component';
import { PropertyViewerFilterComponent } from './components/propertyInspector/property-viewer-filter.component';
import { FileManagerComponent } from './components/fileManager/file-manager.component';
import { GraphViewerComponent } from './components/graphViewer/graph-viewer.component';
import { ChartComponent } from './components/charts/chart.component';
import { HeatMapComponent, PropertyDialog } from './components/charts/heat-map.component';
import { DiffViewerComponent } from './components/diffViewer/diff-viewer.component';
import { DiffPropertyComponent } from './components/diffViewer/diff-property.component';
import { StartScreenComponent } from './components/startScreen/start-screen.component';

// Services
import { DataService } from './services/data.service';
import { InitializationService } from './services/initialization.service';
import { ParserService } from './services/parser.service';
import { FilterService } from './services/filter.service';
import { StatechartService } from './services/statechart.service';
import { ComparisonService } from './services/comparison.service';
import { VisibilityService } from './services/visibility.service';

// Pipes
import { MapValuesPipe } from './utils/pipes/map-values.pipe';
import { SortPropertiesPipe } from './utils/pipes/sort-properties.pipe';

// Routing
import { routing,
         appRoutingProviders } from './app.routing';



@NgModule({
  entryComponents:[
    PropertyDialog
  ],
  declarations: [
    AppComponent,
    StartScreenComponent,
    PropertyViewerComponent,
    PropertyFilterComponent,
    PropertyViewerFilterComponent,
    GraphViewerComponent,
    ChartComponent,
    HeatMapComponent,
    PropertyDialog,
    DiffViewerComponent,
    DiffPropertyComponent,
    FileManagerComponent,
    MapValuesPipe,
    SortPropertiesPipe
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    HttpModule,
    MaterialModule,
    MdCoreModule,
    MdToolbarModule,
    MdTooltipModule,
    MdButtonModule,
    MdSidenavModule,
    MdListModule,
    MdGridListModule,
    MdCardModule,
    MdInputModule,
    MdCheckboxModule,
    MdRadioModule,
    MdIconModule,
    MdMenuModule,
    routing
  ],
  providers: [ DataService, InitializationService, FilterService, ParserService, StatechartService, appRoutingProviders, ComparisonService, VisibilityService ],
  bootstrap: [AppComponent]
})
export class AppModule { }


