oldMode == init & speedIn == lowSpeed & onOff == 1.0 -> mode == init ### support: 320959 confidence: 1.0 coverage: 320959 lift: 0.0
oldMode == init & onOff == 1.0 & decelSet == 0.0 -> mode == init ### support: 478948 confidence: 1.0 coverage: 478948 lift: 0.0
cancel == 1.0 & oldMode == init & onOff == 1.0 -> mode == init ### support: 479533 confidence: 1.0 coverage: 479533 lift: 0.0
oldMode == init & brake == 1.0 & onOff == 1.0 -> mode == init ### support: 479421 confidence: 1.0 coverage: 479421 lift: 0.0
oldMode == init & gas == 1.0 & onOff == 1.0 -> mode == init ### support: 478501 confidence: 1.0 coverage: 478501 lift: 0.0
gas == 0.0 & brake == 0.0 & speedIn == highSpeed & oldMode == active & onOff == 1.0 & cancel == 0.0 -> active == 1.0 ### support: 1821 confidence: 1.0 coverage: 1821 lift: 0.0
gas == 0.0 & decelSet == 1.0 & oldMode == init & brake == 0.0 & speedIn == highSpeed & onOff == 1.0 & cancel == 0.0 -> active == 1.0 ### support: 39921 confidence: 1.0 coverage: 39921 lift: 0.0
gas == 0.0 & accelResume == 0.0 & decelSet == 1.0 & brake == 0.0 & speedIn == highSpeed & oldMode == inactive & onOff == 1.0 & cancel == 0.0 -> active == 1.0 ### support: 401 confidence: 1.0 coverage: 401 lift: 0.0
gas == 0.0 & accelResume == 1.0 & brake == 0.0 & speedIn == highSpeed & oldMode == inactive & onOff == 1.0 & cancel == 0.0 & decelSet == 0.0 -> active == 1.0 ### support: 367 confidence: 1.0 coverage: 367 lift: 0.0
speedIn == lowSpeed -> active == 0.0 ### support: 1338213 confidence: 1.0 coverage: 1338213 lift: 0.0
oldMode == off -> active == 0.0 ### support: 2004212 confidence: 1.0 coverage: 2004212 lift: 0.0
gas == 1.0 -> active == 0.0 ### support: 1999881 confidence: 1.0 coverage: 1999881 lift: 0.0
onOff == 0.0 -> active == 0.0 ### support: 2000172 confidence: 1.0 coverage: 2000172 lift: 0.0
brake == 1.0 -> active == 0.0 ### support: 1999421 confidence: 1.0 coverage: 1999421 lift: 0.0
cancel == 1.0 -> active == 0.0 ### support: 2001203 confidence: 1.0 coverage: 2001203 lift: 0.0
oldMode == init & decelSet == 0.0 -> active == 0.0 ### support: 957575 confidence: 1.0 coverage: 957575 lift: 0.0
accelResume == 0.0 & oldMode == inactive & decelSet == 0.0 -> active == 0.0 ### support: 9406 confidence: 1.0 coverage: 9406 lift: 0.0
accelResume == 1.0 & decelSet == 1.0 & oldMode == inactive -> active == 0.0 ### support: 9496 confidence: 1.0 coverage: 9496 lift: 0.0
gas == 0.0 & brake == 0.0 & speedIn == highSpeed & oldSpeed == lowSpeed & oldMode == active & onOff == 1.0 & cancel == 0.0 -> throttleDelta == belowZero ### support: 659 confidence: 1.0 coverage: 659 lift: 0.0
gas == 0.0 & decelSet == 1.0 & oldMode == init & brake == 0.0 & speedIn == highSpeed & oldSpeed == lowSpeed & onOff == 1.0 & cancel == 0.0 -> throttleDelta == belowZero ### support: 13522 confidence: 1.0 coverage: 13522 lift: 0.0
gas == 0.0 & accelResume == 0.0 & decelSet == 1.0 & brake == 0.0 & speedIn == highSpeed & oldSpeed == lowSpeed & oldMode == inactive & onOff == 1.0 & cancel == 0.0 -> throttleDelta == belowZero ### support: 146 confidence: 1.0 coverage: 146 lift: 0.0
gas == 0.0 & accelResume == 1.0 & brake == 0.0 & speedIn == highSpeed & oldSpeed == lowSpeed & oldMode == inactive & onOff == 1.0 & cancel == 0.0 & decelSet == 0.0 -> throttleDelta == belowZero ### support: 125 confidence: 1.0 coverage: 125 lift: 0.0
cancel == 1.0 -> throttleDelta == zero ### support: 2001203 confidence: 1.0 coverage: 2001203 lift: 0.0
onOff == 0.0 -> throttleDelta == zero ### support: 2000172 confidence: 1.0 coverage: 2000172 lift: 0.0
speedIn == lowSpeed -> throttleDelta == zero ### support: 1338213 confidence: 1.0 coverage: 1338213 lift: 0.0
brake == 1.0 -> throttleDelta == zero ### support: 1999421 confidence: 1.0 coverage: 1999421 lift: 0.0
oldMode == off -> throttleDelta == zero ### support: 2004212 confidence: 1.0 coverage: 2004212 lift: 0.0
gas == 1.0 -> throttleDelta == zero ### support: 1999881 confidence: 1.0 coverage: 1999881 lift: 0.0
oldMode == init & decelSet == 0.0 -> throttleDelta == zero ### support: 957575 confidence: 1.0 coverage: 957575 lift: 0.0
accelResume == 0.0 & oldMode == inactive & decelSet == 0.0 -> throttleDelta == zero ### support: 9406 confidence: 1.0 coverage: 9406 lift: 0.0
accelResume == 1.0 & decelSet == 1.0 & oldMode == inactive -> throttleDelta == zero ### support: 9496 confidence: 1.0 coverage: 9496 lift: 0.0
gas == 0.0 & brake == 0.0 & speedIn == highSpeed & oldMode == active & onOff == 1.0 & cancel == 0.0 -> mode == active ### support: 1821 confidence: 1.0 coverage: 1821 lift: 0.0
gas == 0.0 & decelSet == 1.0 & oldMode == init & brake == 0.0 & speedIn == highSpeed & onOff == 1.0 & cancel == 0.0 -> mode == active ### support: 39921 confidence: 1.0 coverage: 39921 lift: 0.0
gas == 0.0 & accelResume == 0.0 & decelSet == 1.0 & brake == 0.0 & speedIn == highSpeed & oldMode == inactive & onOff == 1.0 & cancel == 0.0 -> mode == active ### support: 401 confidence: 1.0 coverage: 401 lift: 0.0
gas == 0.0 & accelResume == 1.0 & brake == 0.0 & speedIn == highSpeed & oldMode == inactive & onOff == 1.0 & cancel == 0.0 & decelSet == 0.0 -> mode == active ### support: 367 confidence: 1.0 coverage: 367 lift: 0.0
brake == 1.0 & oldMode == active & onOff == 1.0 -> mode == inactive ### support: 10574 confidence: 1.0 coverage: 10574 lift: 0.0
speedIn == lowSpeed & oldMode == active & onOff == 1.0 -> mode == inactive ### support: 7077 confidence: 1.0 coverage: 7077 lift: 0.0
brake == 1.0 & oldMode == inactive & onOff == 1.0 -> mode == inactive ### support: 9163 confidence: 1.0 coverage: 9163 lift: 0.0
gas == 1.0 & oldMode == active & onOff == 1.0 -> mode == inactive ### support: 10566 confidence: 1.0 coverage: 10566 lift: 0.0
cancel == 1.0 & oldMode == inactive & onOff == 1.0 -> mode == inactive ### support: 9264 confidence: 1.0 coverage: 9264 lift: 0.0
gas == 1.0 & oldMode == inactive & onOff == 1.0 -> mode == inactive ### support: 9352 confidence: 1.0 coverage: 9352 lift: 0.0
speedIn == lowSpeed & oldMode == inactive & onOff == 1.0 -> mode == inactive ### support: 6218 confidence: 1.0 coverage: 6218 lift: 0.0
cancel == 1.0 & oldMode == active & onOff == 1.0 -> mode == inactive ### support: 10670 confidence: 1.0 coverage: 10670 lift: 0.0
accelResume == 0.0 & oldMode == inactive & onOff == 1.0 & decelSet == 0.0 -> mode == inactive ### support: 4680 confidence: 1.0 coverage: 4680 lift: 0.0
accelResume == 1.0 & decelSet == 1.0 & oldMode == inactive & onOff == 1.0 -> mode == inactive ### support: 4716 confidence: 1.0 coverage: 4716 lift: 0.0
onOff == 0.0 -> mode == off ### support: 2000172 confidence: 1.0 coverage: 2000172 lift: 0.0
